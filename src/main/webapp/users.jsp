<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.sample.User" %>
<html>
<body>
<h2>User</h2>
<%!
public String showUser(User newUser)
{
	return (newUser.getName());
}
%>
<%
User newUser = new User("Mouse", "9008777777");
System.out.println(showUser(newUser));
%>

<script>

function showNewUser2(user)
{
	alert(user);
}

function showUser()
{
	var user = "newuser2";
	showNewUser2(user);
}

function createUser(userName)
{
	var user = userName;
	alert("user created again: " + userName);
}

function showNewUser()
{
	return showUser();
}

</script>
</body>
</html>
