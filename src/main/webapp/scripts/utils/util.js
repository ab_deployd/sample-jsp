import { getUpperCase2 } from '../utils/util2.js';
export default function getUpperCase(value)
{
	return getUpperCase2(value);
}